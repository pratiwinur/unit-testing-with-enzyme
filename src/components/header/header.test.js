import React from 'react';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from './index';

configure({
    adapter : new Adapter()
})


describe('Header Component', ()=>{
    it('it should be rendered without errors', () =>{
       const component = shallow(<Header/>)
       const wrapper=component.find('.headerComponent');
       expect(wrapper.length).toBe(1); 
    });
});